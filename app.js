const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const cors = require('cors');
const bcrypt = require('bcrypt');
const env = require('dotenv/config');
const UserInfo = require('./models/userInfo');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const app = express();
mongoose.set('debug', true);
app.use(cors());

mongoose.Promise = global.Promise;

//conectar a base
mongoose.connect(process.env.DB_CONNECTION,
  { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false })
  .then(() => console.log('connection succesful'))
  .catch((err) => console.error(err));


//defenição das rotas
const usersInfoRouter = require('./routes/usersInfo');
const testRouter = require('./routes/tests');
const requestRouter = require('./routes/request');
const adminRouter = require('./routes/adminSettings');

app.listen(3000);



app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));


//utilização das rotas
app.use('/usersinfo', usersInfoRouter);
app.use('/tests', testRouter);
app.use('/requests', requestRouter);
app.use('/admin', adminRouter);

//gerenciador basico de erros
app.use(function (err, req, res, next) {
  return res.status(500).json(err);
});
//Criar admin caso seja pagado da base de dados
UserInfo.findOne({ email: "Admin" }, function (err, user) {
  if (err) {
    next(err);
  }
  else if (user == null) {
    var hashedpassword = bcrypt.hashSync("Admin", 10)
    var user = new UserInfo({
      name: "Admin",
      role: 2,
      email: "Admin",
      password: hashedpassword,
      sex: "unknown",
      age: 1,
      contact: 900000000,
      address: "unknown",
      diseases: "unknown",
      status: 0
    })
    user.save(function (err, next) {
      if (err) {
        next(err);
      }
    });
  }
})

module.exports = app;
