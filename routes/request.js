const express = require('express');
const router = express.Router();
const requestController = require("../controllers/requestController.js");
const userInfoController = require("../controllers/userInfoController");
const Verification = require("../routes/verifyTokens.js");
//routes relativas a pedidos
router.post('/createRequest', (req, res, next) => { Verification(req, res, next, process.env.userrrole) }, requestController.createRequest);
router.get('/allrequests', (req, res, next) => { Verification(req, res, next, process.env.tecnicrole) }, requestController.getAllRequests);
router.get('/onerequest/:requestCode', (req, res, next) => { Verification(req, res, next, process.env.userrole) }, requestController.getOneRequest);
router.delete('/deleteRequest/:requestCode', (req, res, next) => { Verification(req, res, next, process.env.userolle) }, requestController.deleteRequest);

router.get('/myrequests', (req, res, next) => { Verification(req, res, next, process.env.userrrole) }, requestController.myRequests);
router.post('/acceptrequest/:requestCode', (req, res, next) => { Verification(req, res, next, process.env.tecnicrole) }, requestController.acceptRequest);
router.delete('/deleteRequests/:userId',(req, res, next) => { Verification(req, res, next, process.env.tecnicrole) }, requestController.deleteAllRequestUser);

router.param('requestCode',requestController.getByCodeRequest);
router.param('userId',userInfoController.getByIdUser);

module.exports = router;