const express = require('express');
const router = express.Router();
const Verification = require("../routes/verifyTokens.js");
const userInfoController = require("../controllers/userInfoController.js");
const adminController = require('../controllers/adminController.js');

router.post('/testsperday', (req, res, next) => { Verification(req, res, next, process.env.adminrole) }, adminController.getTestsPerDay);
router.get('/testsperperson/:userId', (req, res, next) => { Verification(req, res, next, process.env.adminrole) }, adminController.getTestsPerPerson);
router.get('/infected', (req, res, next) => { Verification(req, res, next, process.env.adminrole) }, adminController.getNumberInfected);

router.param('userId', userInfoController.getByIdUser);

module.exports = router;