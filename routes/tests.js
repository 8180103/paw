const express = require('express');
const router = express.Router();
const testController = require("../controllers/testController.js");
const userInfoController = require("../controllers/userInfoController");
const Verification = require("../routes/verifyTokens.js");

//routes relativas a testes
router.get('/alltests', (req, res, next) => { Verification(req, res, next, process.env.userrrole) }, testController.getAllTests);
router.post('/createtest', (req, res, next) => { Verification(req, res, next, process.env.tecnicrole) }, testController.createTest);
router.put('/updatetest/:testCode', (req, res, next) => { Verification(req, res, next, process.env.tecnicrole) }, testController.updateTest);
router.delete('/deleteTest/:testCode', (req, res, next) => { Verification(req, res, next, process.env.tecnicrole) }, testController.deleteTest);
router.get('/onetest/:testCode', (req, res, next) => { Verification(req, res, next, process.env.userrrole) }, testController.getOneTest);
router.get('/getfile/:testCode', (req, res, next) => { Verification(req, res, next, process.env.userrrole) },testController.getFile)
router.get('/mytests', (req, res, next) => { Verification(req, res, next, process.env.userrrole) }, testController.myTests);
router.get('/usertests',(req, res, next) => { Verification(req, res, next, process.env.tecnicrole) },testController.userTests);

router.param('testCode',testController.getByCodeTest);
router.param('testId', testController.getByIdTest);
router.param('userId', userInfoController.getByIdUser);

module.exports = router;