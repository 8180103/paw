const express = require('express');
const router = express.Router();
const userInfoController = require("../controllers/userInfoController.js");
const Verification = require("../routes/verifyTokens.js");
const uploadController = require ("../controllers/uploadController");

//routes relativas a users
router.get('/users', (req,res,next)=>{Verification(req,res,next,process.env.tecnicrole)},userInfoController.getAllUsers);
router.post('/create', userInfoController.createUser);
router.post('/createTecnico', (req,res,next)=>{Verification(req,res,next,process.env.tecnicroleadminrole)}, userInfoController.createUserTecnico);
router.post('/login', userInfoController.login);

router.post('/upload', (req, res, next) => { Verification(req, res, next, process.env.userrole) } ,uploadController.uploadFile);

router.get('/user/:userId',(req,res,next)=>{Verification(req,res,next,process.env.userrole)}, userInfoController.getOneUser);
router.put('/user/:userId',(req,res,next)=>{Verification(req,res,next,process.env.userrole)}, userInfoController.updateUser);
router.delete('/user/:userId',(req,res,next)=>{Verification(req,res,next,process.env.userrole)}, userInfoController.deleteUser);

router.param('userId', userInfoController.getByIdUser);

module.exports = router;