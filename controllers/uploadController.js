const upload = require("../routes/upload");

const uploadFile = async (req, res) => {
  try {
    await upload(req, res);
    if (req.file == undefined) {
      return res.status(400).json(`Por favor, seleciona um ficheiro.`);
    }
    return res.status(200).json(`Ficheiro submetido.`);
  } catch (err) {
    return res.status(500).json(`Erro ao fazer upload do ficheiro.`);
  }
};

module.exports = {
  uploadFile: uploadFile
};