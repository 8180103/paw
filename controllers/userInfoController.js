const mongoose = require('mongoose');
const UserInfo = require("../models/userInfo");
const bcrypt = require('bcrypt')
const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
const jwt = require('jsonwebtoken');


var userInfoController = {};
//Criar user sem verificação de login
userInfoController.createUser = function (req, res) {
    try {
        if ((req.body.name).length == 0 || (req.body.sex).length == 0 || (req.body.address).length == 0 || (req.body.email).length == 0 || (req.body.password).length == 0 || (req.body.contact).length != 9) {
            return res.status(400).json("Introduza campos válidos");
        } else if (req.body.role != undefined) {
            return res.status(403).json("Não tem permissão para tal");
        } else {
            UserInfo.findOne({ email: req.body.email }, async function (err, user) {
                if (err) {
                    return res.status(400).json("Introduza o email");
                } else {
                    if (user != undefined) {
                        return res.status(406).json("Email existente");
                    } else {
                        req.body.password = bcrypt.hashSync(req.body.password, 10)
                        var user = new UserInfo(req.body)
                        user.save(function (err, next) {
                            if (err) {
                                return res.status(400).json("Introduza o email");
                            }
                        }
                        ); const token = jwt.sign({ _id: user._id, role: user.role, name: user.name }, process.env.TOKEN_SECRET, { expiresIn: 1440 })
                        return res.status(201).json({ user: user, token: token })
                    }
                }
            });
        }
    } catch{
        return res.status(400).json("Introduza campos válidos");
    }
}




//Criar user com verificação de login
userInfoController.createUserTecnico = function (req, res) {
    try {
        if ((req.body.name).length == 0 || (req.body.address).length == 0 || (req.body.email).length == 0 || (req.body.password).length == 0) {
            return res.status(400).json("Introduza campos válidos");
        } else if (req.body.role > 2 || req.body.role < 0) {
            return res.status(403).json("Introduza um role adequada");
        } else {
            UserInfo.findOne({ email: req.body.email }, async function (err, user) {
                if (err) {
                    return res.status(400).json("Introduza o email");
                } else {
                    if (user != undefined) {
                        return res.status(400).json("User já existe");
                    } else {
                        try {
                            req.body.password = bcrypt.hashSync(req.body.password, 10)
                            var user = new UserInfo(req.body)
                            user.save(function (err, next) {
                                if (err) {
                                    return res.status(400).json("Introduza o email");
                                }
                            }
                            );
                            res.status(201).json("Utilizador gravado com sucesso " + user)
                        } catch (err) {
                            return res.status(400).json("Introduza campos válidos");
                        }
                    }
                }
            })
        }
    } catch{
        return res.status(400).json("Introduza campos válidos");
    }
}

//Login
userInfoController.login = function (req, res) {
    if ((req.body.email).length == 0 || (req.body.password).length == 0 || req.body.password == undefined || req.body.email == undefined) {
        return res.status(400).json("Introduza campos válidos");
    }
    UserInfo.findOne({ email: req.body.email }, async function (err, user) {
        if (err) {
            return res.status(400).json("Introduza o email");
        } else {
            if (user == null) {
                return res.status(400).json("Introduza o email");
            } else {
                var valido = await bcrypt.compare(req.body.password, user.password);
                if (!valido) {
                    return res.status(400).json("Credenciais invalidas");
                } else {
                    const token = jwt.sign({ _id: user._id, role: user.role, name: user.name }, process.env.TOKEN_SECRET, { expiresIn: 1440 })
                    return res.status(202).json({ user: user, token: token });
                }
            }
        }
    });
};

//Mudar password ao Admin
userInfoController.updateAdmin = function (req, res) {
   
    if ( req.body.password == undefined) {
        return res.status(200).json("Introduza campos válidos")
    }else{
            req.body.password = bcrypt.hashSync(req.body.password, 10);
        
    } UserInfo.findOneAndUpdate({ email: "Admin" }, req.body.password, { new: true }, function (err, user) {
        if (err) {
            return res.status(400).json("Erro na base de dados");
        } else {
            return res.status(200).json("Credenciais mudadas");
        }
    });
};

//update user
userInfoController.updateUser = function (req, res) {
    if (req.body.role!= undefined) {
        return res.status(400).json("Erro não tem acesso a esse tipo de alterações");
           }
           if ( req.body.password != undefined) {
          req.body.password = bcrypt.hashSync(req.body.password, 10);
            
        }
    if (req.body.email == undefined) {
        UserInfo.findByIdAndUpdate(req.user._id, req.body, { new: true }, function (err, user) {
            if (err) {
                return res.status(400).json("Erro");
            } else {
                return res.status(200).json(user);
            }
        }
        )
    } else {
        UserInfo.findOne({ email: req.body.email }, async function (err, user) {
            if (err) {
                return res.status(500).json("Erro");
            } else {
                if (user != null) {
                    return res.status(400).json("Ja existe esse email registado");
                } else {
                    UserInfo.findByIdAndUpdate(req.user._id, req.body, { new: true }, function (err, user) {
                        if (err) {
                            return res.status(400).json("Introduza todos os parametros");
                        } else {
                            return res.status(200).json(user);
                        }
                    }
                    )
                }
            }
        })
    }
}

//apagar User
userInfoController.deleteUser = function (req, res, next) {
    req.user.remove(function (err, user) {
        if (err) {
            return res.status(400).json("Impossivel remover user");
        } else {
            return res.status(200).json("User deleted");
        }
    });
};


//Ver users
userInfoController.getAllUsers = function (req, res, next) {
    UserInfo.find(function (err, users) {
        if (err) {
            return res.status(400).json("Introduza o email");
        } else {
            if (users == undefined) {
                return res.status(400).json("User não existe");
            } else {
                return res.status(200).json(users);
            }
        }
    });
};


//encontrar um user por id e listar
userInfoController.getOneUser = function (req, res) {
    return res.status(200).json(req.user);
};

//encontrar user por id
userInfoController.getByIdUser = function (req, res, next, id) {
    UserInfo.findById({ _id: id }, function (err, user) {
        if (err) {
            return res.status(400).json("Introduza o id válido");
        } else {
            if (user == undefined) {
                return res.status(400).json("User não existe");
            } else {
                req.user = user
                next();
            }
        }
    }
    );
}
module.exports = userInfoController;