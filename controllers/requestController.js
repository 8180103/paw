const mongoose = require('mongoose');
const UserInfo = require('../models/userInfo');
const Request = require('../models/request');
const Test = require('../models/test');

requestController = {};

/**Cria Request */
requestController.createRequest = function (req, res, next) {
    var request = new Request(req.body);
    request.code = generateCode();
    request.userid = req.userid;
    //PROCURA O NOME DO USER
    UserInfo.findById({ _id: req.userid }, function (err, user) {
        if (err) {
            next(err);
        } else {
            request.name = user.name;
            request.save(function (err) {
                if (err) {
                    next(err);
                } else {
                    res.json(request);
                }
            });
        }
    });
};


/**Todos os requests para fazer um test*/
requestController.getAllRequests = function (req, res, next) {
    Request.find(function (err, requests) {
        if (err) {
            next(err);
        } else {
            res.json(requests);
        }
    });
};

/**Elimina o request, considerando que este foi aceite*/
requestController.acceptRequest = function (req, res, next) {

    var temp = {
        date: req.body.date,
        userid: req.request.userid,
        client: req.request.name,
        technician: req.username
    }
    var test = new Test(temp);

    test.code = req.request.code;

    if (req.body.date == null) {
        res.json("Coloque uma data.");
    } else {
        Test.find({ userid: req.request.userid }, function (err, tests) {
            if (err) {
                next(err);
            } else {
                if (tests.length == 0) {
                    test.utilstatus = 1;
                    test.save(function (err) {
                        if (err) {
                            next(err);
                        } else {
                            Request.deleteMany({ userid: req.request.userid }, function (err) {
                                if (err) next(err);
                                else res.json(test);
                            });
                        }
                    });
                } else {

                    var date = Date.parse(req.body.date);
                    var mais = addMin(2880, date);
                    var menos = takeMin(2880, date);

                    const verificationDate = verifyDate(tests, menos, mais);

                    if (verificationDate) {
                        res.json("Impossivel marcar a essa hora");
                    } else {
                        UserInfo.findById({ _id: req.request.userid }, function (err, user) {
                            if (err) {
                                next(err);
                            } else {
                                test.utilstatus = user.status;

                                test.save(function (err) {
                                    if (err) {
                                        next(err);
                                    } else {
                                        Request.deleteMany({ userid: req.request.userid }, function (err) {
                                            if (err) next(err);
                                            else res.json(test);
                                        });
                                    }
                                });
                            }
                        });
                    }
                }
            }
        });
    }
};


/** */
requestController.getByCodeRequest = function (req, res, next, code) {
    Request.findOne({ code: code }, function (err, request) {
        if (err) {
            next(err);
        } else {
            req.request = request;
            next();
        }
    });
};

/**Elimina um Request especifico*/
requestController.deleteRequest = function (req, res, next) {
    req.request.remove(function (err) {
        if (err) {
            next(err);
        } else {
            res.json(req.request);
        }
    });
};

/**Elimina todos os requests feitos por um user*/
requestController.deleteAllRequestUser = function (req, res, next) {
    Request.deleteMany({ userid: req.user._id }, function (err) {
        if (err) next(err);
        else res.json("Pedidos Eliminados");
    });
};

/**Encontra um Request*/
requestController.getOneRequest = function (req, res, next) {
    res.json(req.request);
};

/**Seus próprios testes */
requestController.myRequests = function (req, res, next) {
    Request.find({ userid: req.userid }, function (err, requests) {
        if (err) {
            next(err);
        } else {
            res.json(requests);
        }
    });
};

/**Gera uma string aleatória*/
function generateCode() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 10; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};


/**Verifica se existe um teste proximo do dado */
function verifyDate(tests, menos, mais) {
    for (var i = 0; i < tests.length; i++) {
        if (Date.parse(tests[i].date) >= menos && Date.parse(tests[i].date) <= mais) {
            return true;
        }
    }
    return false;
};

/**Adiciona minutos a uma data*/
function addMin(min, date) {
    date = date + (min * 60 * 1000);
    return date;
};

/**Tira minutos a uma data*/
function takeMin(min, date) {
    date = date - (min * 60 * 1000);
    return date;
};

module.exports = requestController;
