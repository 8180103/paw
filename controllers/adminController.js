const mongoose = require('mongoose');
const UserInfo = require('../models/userInfo');
const Test = require('../models/test');

adminController = {};

adminController.getTestsPerDay = function (req, res, next) {
    var newdate1 = new Date(req.body.date);

    Test.find({ date: { "$gte": new Date(newdate1.setHours(00, 00, 00)), "$lt": new Date(newdate1.setHours(23, 59, 59)) } }, function (err, result) {
        if (err) {
            res.status(400).json(err);
        } else {
            res.status(200).json("Número de testes em " + req.body.date + " : " + result.length);
        }
    });
};

adminController.getTestsPerPerson = function (req, res, next) {
    Test.find({ userid: req.user._id }, function (err, result) {
        if (err) {
            res.status(400).json(err);
        } else {
            if (result.length == 0) {
                res.status(200).json("O user dado não fez testes.");
            } else {
                if (result.length == 1) {
                    res.status(200).json(result[0].client + " realizou " + result.length + " teste.");
                } else {
                    res.status(200).json(result[0].client + " realizou " + result.length + " testes.");
                }
            }
        }
    });
};

adminController.getNumberInfected = function (req, res, next) {
    UserInfo.find({ status: 2 }, function (err, result) {
        if (err) {
            res.status(400).json(err);
        } else {
            res.status(200).json("Número total de infetados: " + result.length);
        }
    });
};

module.exports = adminController;
